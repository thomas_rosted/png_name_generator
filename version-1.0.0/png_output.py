#!/usr/bin/python
# -*- coding: utf-8 -*-

#
# ----------------------------------------------------------------------------
# "THE BEER-WARE LICENSE" (Revision 42):
# <thomas.rosted@protonmail.com> wrote this file.  As long as you retain this
# notice you can do whatever you want with this stuff. If we meet some day,
# and you think this stuff is worth it, you can buy me a beer in return.
# - Thomas Rosted Larsen
#
# Credit to Poul-Henning Kamp, who I will probably buy a beer whenever.
# ----------------------------------------------------------------------------
#
# This script handles most output to console for png_name_generator.py
# written and tested on Ubuntu Disco using Python 3.7.3
#

# version
version_text = '\n{name} version {version}\n'

# help text
help_text  = '\n'
help_text += "-l or --list:         followed by INT number of names to be generated (default = 1)\n"
help_text += "                      example: -l 25 or --list 25 for a list with 25 names\n"
help_text += "-cc or --language:    STR country code or language for name(s) requested\n"
help_text += "                      current choices: de (german), da (danish), eng (english)\n"
help_text += "                      examples: -da or --danish for danish names\n"
help_text += "-o or --output:       BOOL write to file (default = console, destination = default)\n"
help_text += "                      example 1: write 10 danish names to default file output ~/resultfile.txt'\n"
help_text += "                      ~/png_name_generator.py --output --danish --list 10\n"
help_text += "                      example 2: write 10 danish names to file /path/to/myfile.txt'\n"
help_text += "                      ~/png_name_generator.py -o ~/path/to/myfile.txt -da -l 10\n"
# help_text += "-r or --request:      ENUM,STR choose only from [firstnames, middlenames, surnames] beginning with STR (default = NONE)\n"
# help_text += "                      {f:STR:...:STR},{m:STR:...:STR},{s:STR:...:STR} example: --request f:theodolf:arnulf\n"
# help_text += "                      note: middlename/surname are combined to form a 2-part proper surname\n"
# help_text += "                      if no matches are found, all names will be used\n"
# help_text += "                      NOTE: requests module currently inactive\n"


# not implemented yet
country_help  = 'list of working countrycodes:\n'
country_help += 'da, de, eng'


# writing to file
create_file_success = 'succesfully created file {name}'
create_file_failure = 'failed to create file {name}'
write_success = '{names} {lang} names succesfully written to file {outfile}'
write_failure = 'error writing to file'
no_destination = 'no destination supplied, using default'
outfile_error = 'failed to verify any destination, exiting'
destination_rejected = 'destination rejected, using default'
permission_error = 'failed to create parent directories'
output_status = 'output to file {outfile}'

# country updates
country_update_success = 'succesfully added {country} to results'
country_update_failure = 'failed to update country code to {country}'
no_country_error = 'no country code supplied, using default language {country}'
working_language = 'generating {lang} names'

# requirements updates
req_index_error = 'missing requirements argument'
fixed_req = 'requirement {original_req} changed to {fixed_req}'

# other
error_converting_to_integer = ''

# task related
task_details = 'added task: {count} {lang} names to {out}'

def print_task_details(task, language):
    if task['output'] == 'console': dest = 'console'
    else: dest = task['outfile']
    to_print = task_details.format(count=task['items'],lang=language,out=dest)
    print(to_print)

def print_version(vname,vversion):
    to_print = version_text.format(name=vname,version=vversion)
    print(to_print)

def print_help_text():
    print(help_text)

def print_create_file_success(filename):
    to_print = create_file_success.format(name=filename)
    print(to_print)

def print_create_file_failure(filename):
    to_print = create_file_failure.format(name=filename)
    print(to_print)

def print_write_success(filename, language, count):
    to_print = write_success.format(outfile=filename, lang=language, names=count)
    print(to_print)

def print_write_failure():
    to_print = write_failure
    print(to_print)

def print_no_destination():
    to_print = no_destination
    print(to_print)

def print_outfile_error():
    to_print = outfile_error
    print(to_print)

def print_destination_rejected():
    to_print = destination_rejected
    print(to_print)

def print_permission_error():
    to_print = permission_error
    print(to_print)

def print_output_status(out):
    to_print = output_status.format(outfile=out)
    print(to_print)


def print_country_update_success(countrycode):
    to_print = country_update_success.format(country=countrycode)
    print(to_print)

def print_country_update_failure(countrycode):
    to_print = country_update_failure.format(country=countrycode)
    print(to_print)

def print_no_country_error(countrycode):
    to_print = no_country_error.format(country=countrycode)
    print(to_print)

def print_language_status(language):
    to_print = working_language.format(lang=language)
    print(to_print)


def print_req_index_error():
    to_print = req_index_error
    print(to_print)

def print_fixed_req(old_req, new_req):
    to_print = fixed_req.format(original_req=old_req, fixed_req=new_req)
    print(to_print)

