#!/usr/bin/python
# -*- coding: utf-8 -*-

#
# ----------------------------------------------------------------------------
# "THE BEER-WARE LICENSE" (Revision 42):
# <thomas.rosted@protonmail.com> wrote this file.  As long as you retain this
# notice you can do whatever you want with this stuff. If we meet some day,
# and you think this stuff is worth it, you can buy me a beer in return.
# - Thomas Rosted Larsen
#
# Credit to Poul-Henning Kamp, who I will probably buy a beer whenever.
# ----------------------------------------------------------------------------
#
# This script will generate x names inspired by a language of the users choice.
# See description for adding names or languages in the file png_listhandler.py
# run /path/to/script/png_name_generator.py --help for further information
# written and tested on Ubuntu Disco using Python 3.7.3
#
# STATUS 2020.01.31: good but not quite there yet
# code cleanup needed, (some) functionality to implement, some syntax stuff
#

import sys, os, random
import png_output as output
import png_listhandler as listhandler
from time import sleep

# version
SCRIPT_NAME = 'PNG Name Generator'
SCRIPT_VERSION = '1.0.0'

# tasks
DEFAULT = {'output':'console','outfile':'resultfile.txt','language':'eng','items':1}
TASKS = []

# global result list
RESULT_LIST = []

# global requirements lists
REQS_FIRST = []
REQS_LAST_1 = []
REQS_LAST_2 = []



# TODO - kill off remaining global parameters
# TODO - API stuff? might as well be able to call this from any script
# TODO - test file/directory creation for unhandled errors - verify_outfile()
# TODO - test permission handling, ensure never to overwrite system files - verify_outfile()
# TODO - implement rules in name generation - name_eval()
# TODO - random nobility - sir firstname of lastname - firstname von lastname
# TODO - entire requests module - verify_and_fix_requests() AND parse_args()
# TODO - message to user - created directories - verify_outfile()
# TODO - message to user - print args error message - boilerplate
# TODO - message to user - once per language - main()
# TODO - cleanup code, delete unused and obsolete

# obsolete
def old_generate_firstname():
    result = ''
    if len(REQS_FIRST) == 0: result = random.choice(listhandler.FIRST_SINGLE_1) # random choice from full list
    else: # if certain first (parts of) first names have been requested
        valid_names = []
        for req in REQS_FIRST: # filter all first (part of) first names and add to new list
            valid_names.extend([name for name in listhandler.FIRST_SINGLE_1 if name.startswith(str(req)) and name not in valid_names])
        result = random.choice(valid_names) # random choice from filtered list 
    if result in listhandler.FIRST_MULTI_1: # some first names deserve an extra name
        result += '-' + random.choice(listhandler.FIRST_MULTI_2) # add a second first name    
    elif random.getrandbits(1): # the remaining first names still get a shot at an additional first name, without the dash though
        while 1:
            extra_name = random.choice(listhandler.FIRST_SINGLE_1) # random choice from full list
            if extra_name != result and extra_name not in listhandler.FIRST_MULTI_1: # test that both names are not identical, otherwise repeat
                result += ' ' + extra_name
                break
    return result

# obsolete
def old_generate_lastname():
    result = nobleman_modifier()
    last1 = last2 = ''
    if len(listhandler.LAST_1) > 0: list_1 = listhandler.LAST_1
    else: list_1 = listhandler.MID_1
    if len(listhandler.LAST_2) > 0: list_2 = listhandler.LAST_2
    else: list_2 = listhandler.MID_2
    if len(REQS_LAST_1) == 0: last1 += random.choice(list_1) # random choice from full list
    else: # if certain first (parts of) last names have been requested
        valid_names = []
        for req in REQS_LAST_1: # filter all first (part of) last names and add to new list
            valid_names.extend([name for name in list_1 if name.startswith(str(req)) and name not in valid_names])
        last1 = random.choice(valid_names) # random choice from filtered list
        
    if len(REQS_LAST_2) == 0: last2 = random.choice(list_2) # random choice from full list
    else: # if certain last (parts of) last names have been requested
        valid_names = []
        for req in REQS_LAST_2: # filter all last (part of) last names and add to new list
            valid_names.extend([name for name in list_2 if name.startswith(str(req)) and name not in valid_names])
        last2 = random.choice(valid_names) # random choice from filtered list

    for _set in listhandler.get_exclusive(): # check rules if different letters are mutually exclusive
        if last1[-1] in _set and last2[0] in _set:
            return old_generate_lastname()

    if last1[-1] == last2[0]: # last starts with same as first ends
        if len(listhandler.get_inclusive()) == 0: # if no rules for inclusiveness
            return old_generate_lastname()
        if not last2[0] in listhandler.get_inclusive(): # check rules if okay
            return old_generate_lastname()

    if last1[-2:] == last2[-2:]: # last two in each identical
        return old_generate_lastname()
    if last1[:2].lower() == last2[:2]: # first two in each identical
        return old_generate_lastname()
    return result + last1 + last2

# obsolete
def nobleman_modifier():
    if COUNTRYCODE == 'de' and random.randrange(20) == 1: 
        return 'von '
    else: return ''


def generate_name():
    structure = random.choice(listhandler.get_structure())
    name = []
    for struct in structure:
        variations, classifiers = listhandler.get_variations(struct)
        name.append(get_name_part(variations, classifiers, struct))
    return make_name(name, ' ')


def get_name_part(variations, classifiers, struct):
    name_part = ['']
#    name_part.append('')
    delimiter = ''
    for i, c in enumerate(classifiers):
        valid_names = listhandler.get_valid_names(c)
        nice_name = False
        while not nice_name:
            resultset = random.choice(valid_names)
            valid_variations = []
            for v in variations:
                if len(v) -1 < i: continue
                if resultset[1] not in v[i]: continue
                if resultset[1] not in c: continue
                valid_variations.append(v)
            nice_name = name_eval(name_part[-1], resultset[0])
            var = random.choice(valid_variations)
        name_part.append(resultset[0])
        if len(var) -1 == i: break
        else: delimiter = listhandler.get_delimiter(struct)
    for i, part in enumerate(name_part):
        if part.endswith('*'):
            options = listhandler.get_additives(part[:-1])
            name_part[i] = random.choice(options)
    return make_name(name_part[1:], delimiter)


# TODO - implement even more rules in name generation - probably an infinite todo
def name_eval(current, addition):
    if current == '': return True
    # check rules if different letters are mutually exclusive
    for _set in listhandler.get_exclusive():
        if current[-1] in _set and addition[0] in _set: # compare last v first letter
            return False
    # check rules if repetitions okay
    if current[-1] == addition[0]: # last starts with same as first ends
        if addition[0] not in listhandler.get_inclusive(): # check rules if okay
            return False
    # standard rule - both are forbidden to start or end with the same two
    if current[-2:].lower() == addition[-2:].lower(): # last two in each identical
        return False
    if current[:2].lower() == addition[:2]: # first two in each identical
        return False
    return True


# add spaces and stuff
def make_name(name_parts, delimiter):
    name = ''
    for name_part in name_parts:
        name += name_part + delimiter
    return name.rstrip(delimiter).title()



# arguments stored in global variables
def parse_args(args):
    listhandler.load_lang()
    current_task = {}
    for i, arg in enumerate(args):
        if arg in ['-h', '--help']:
            output.print_help_text()
            exit()
        if arg in ['-o', '--output']:
            if 'output' in current_task:
                close_task(current_task)
                current_task.clear()
            current_task['output'] = 'file'
            if len(args) -1 > i: # mitigate index error
                if not args[i+1].startswith('-'): # arg, not destination
                    if verify_outfile(args[i+1]): # check if valid destination supplied by user
                        current_task['outfile'] = parse_outfile(args[i+1])
                    else: output.print_destination_rejected()
            else: output.print_no_destination()
        elif arg in ['-l', '--list']:
            if 'items' in current_task:
                close_task(current_task)
                current_task.clear()
            if len(args) -1 > i: # mitigate index error
                current_task['items'] = parse_int(args[i+1]) # check if valid integer supplied by user
        # TODO - entire requests module
        elif arg in ['-r', '--request']:
            if len(args) -1 > i:
                req = args[i+1]
                if req.startswith('f'): # store requested (parts of) first name
                    REQS_FIRST.extend(x.capitalize() for x in req.rsplit(':')[1:])
                if req.startswith('m'): # store requested first (parts of) last name
                    REQS_LAST_1.extend(x.capitalize() for x in req.rsplit(':')[1:])
                if req.startswith('s'): # store requested second (parts of) last name
                    REQS_LAST_2.extend(x.lower() for x in req.rsplit(':')[1:])
            else: output.print_req_index_error()
        elif arg.startswith('--'): # if proper argument, must be a language or invalid - otherwise handled earlier
            if listhandler.valid_language(args[i][2:]): # check for valid language
                if 'language' in current_task:
                    close_task(current_task)
                    current_task.clear()
                current_task['language'] = listhandler.get_cc(args[i][2:]) # add language code to task
        elif arg.startswith('-'): # if proper argument, must be a language code or invalid - otherwise handled earlier
            if listhandler.valid_cc(args[i][1:]): # check for valid language code
                if 'language' in current_task:
                    close_task(current_task)
                    current_task.clear()
                current_task['language'] = args[i][1:]
    if len(current_task) > 0: # finish up task in progress if it contains data
        close_task(current_task)
    print()
    return True


def close_task(_task):
    task = {'output':'console','outfile':'resultfile.txt','language':'eng','items':1}
    if 'output' in _task: task['output'] = _task['output']
    if 'outfile' in _task: task['outfile'] = _task['outfile']
    if 'language' in _task: task['language'] = _task['language']
    if 'items' in _task: task['items'] = _task['items']
    TASKS.append(task)
    lang = listhandler.get_language(task['language'])
    output.print_task_details(task, lang)


def parse_int(i):
    try:
        res = int(i)
        return res
    except ValueError:
        return 1


def parse_outfile(_filename):
    return os.path.join(os.getcwd(), _filename)


# TODO - entire requests module
# rewrite to not use harcoded lists - make getters in listhandler
# check submitted requests and fix invalid ones
def verify_and_fix_requests():
    for req in REQS_FIRST:
        tmp_list = [name for name in listhandler.FULL_FIRST if name.startswith(req)]
        if not len(tmp_list) > 0:
            fix_req(req, req, REQS_FIRST, listhandler.FULL_FIRST)
    for req in REQS_LAST_1:
        tmp_list = [name for name in listhandler.LAST_1 if name.startswith(req)]
        if not len(tmp_list) > 0:
            fix_req(req, req, REQS_LAST_1, listhandler.LAST_1)
    for req in REQS_LAST_2:
        tmp_list = [name for name in listhandler.LAST_2 if name.startswith(req)]
        if not len(tmp_list) > 0:
            fix_req(req, req, REQS_LAST_2, listhandler.LAST_2)


# shorten requirements recursively until a match is found, or delete the empty requirement
def fix_req(original_req, req, reqs, names):
    for i in range(len(reqs)):
        if req == reqs[i]:
            reqs[i] = req[:-1]
            if len(reqs[i]) > 0:
                tmp_list = [name for name in names if name.startswith(reqs[i])]
                if len(tmp_list) > 0: output.print_fixed_req(original_req, reqs[i])
                else: fix_req(original_req, reqs[i], reqs, names)
            else: del reqs[i]
            break


# TODO - test file/directory creation for unhandled errors
# TODO - permission handling, ensure never to overwrite system files
# if output to file and destination supplied, tests if the destination exists and creates a file if it does not
def verify_outfile(_filename):
    if len(_filename.split('.')) < 2: return False # filename must contain some sort of file type
    filename = os.path.join(os.getcwd(), _filename)
    if filename.startswith('-'): # no destination supplied
        return False
    if (len(filename) < 3) or (len(os.path.basename(filename)) < 3): # short filename rejected
        return False 

    if not os.path.isdir(os.path.dirname(filename)): # check if parent directory is a directory
        try:
            os.makedirs(os.path.dirname(filename)) # create necessary directories supplied by the user
            # TODO - message to user - created directories
        except (OSError):
            output.print_permission_error()
            return False
    try:
        with open(filename, 'a'): # creates an empty file
            pass
        output.print_create_file_success(filename) # succesfully created file
        return True
    except (OSError):
        output.print_create_file_failure(filename) # failed to create file
        return False


# write to file
def write_file(dest,lang):
    with open(dest, 'w') as data:
        try:
            for name in RESULT_LIST:
                data.writelines(name+'\n')
            output.print_write_success(dest, lang, len(RESULT_LIST))
        except OSError: output.print_write_failure()


# init and load name DB
def init_lists(lang):
    listhandler.clear_lists()
    RESULT_LIST.clear()
    if listhandler.set_dbdir(): listhandler.loadDB(lang)
    else: exit()


def main():
    for task in TASKS:
        init_lists(task['language'])
        verify_and_fix_requests()
#        output.print_language_status(listhandler.get_language(lang)) # TODO - message to user? - once per language
        for name in range(task['items']):
            RESULT_LIST.append(generate_name()) # create a bunch of names
        if task['output'] == 'file': 
            write_file(task['outfile'],listhandler.get_language(task['language'])) # when not writing to console
        else: [print(name) for name in RESULT_LIST]
        print()


#boilerplate
if __name__ == '__main__':
    output.print_version(SCRIPT_NAME,SCRIPT_VERSION)
    if len(sys.argv) > 1:
        if parse_args(sys.argv[1:]):
            main()
        else: pass # TODO - message to user - print args error message
    else: output.print_help_text()
    exit()

