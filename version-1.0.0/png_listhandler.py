#!/usr/bin/python
# -*- coding: utf-8 -*-

#
# ----------------------------------------------------------------------------
# "THE BEER-WARE LICENSE" (Revision 42):
# <thomas.rosted@protonmail.com> wrote this file.  As long as you retain this
# notice you can do whatever you want with this stuff. If we meet some day,
# and you think this stuff is worth it, you can buy me a beer in return.
# - Thomas Rosted Larsen
#
# Credit to Poul-Henning Kamp, who I will probably buy a beer whenever.
# ----------------------------------------------------------------------------
#
# This script will generate x names inspired by a language of the users choice.
# TODO add description on how to add names/languages
# written and tested on Ubuntu Disco using Python 3.7.3
#

import os

# global
DBDIR = ''
RULES = []
CLASSIFIERS = []
NAMES = []
# LANGUAGES = {'german':'de','danish':'da','english':'eng','finnish':'fi'} # TODO - possible to make generic?
LANG_NEW = {}



# TODO
# update rules with number of names
# names: first, middle, last
# include multiples of each in options

# TODO
# translate countrycodes into language names - done
# add getter function and pipe to output


# used when pairing from two lists. here: different letters exclude matching
def get_exclusive():
    result = []
    for rule in RULES:
        if rule.startswith('exclusive'):
            result.append([r for r in rule.split(':')[1:]])
    return result

# used when pairing from two lists. here: same letters does not exclude matching
def get_inclusive():
    result = []
    for rule in RULES:
        if rule.startswith('inclusive'):
            result.extend(r for r in rule.split(':')[1:])
    return result

# structure defined in rules. example structure:f:m:l (firstname, middlename, lastname), multiple structures allowed
def get_structure():
    result = []
    for rule in RULES:
        if rule.startswith('structure'):
            result.append([r for r in rule.split(':')[1:]])
    return result

# used when pairing from two lists, the delimiter will separate the matches depending on position in structure (default: none)
def get_delimiter(struct):
    for rule in RULES:
        if rule.startswith('delimiter'):
            _set = rule.split(':')
            if _set[1].startswith(struct):
                if _set[2] == 'h': return '-' # returns a hyphen
                if _set[2] == 'b': return ' ' # returns a blank space
                if _set[2] == 'n': return '' # returns an empty string (no delimiter)
                else: continue
    return ''

# returns possible variations and classifiers allowed for these
def get_variations(struct):
    variations = []
    classifiers = []
    tmp_list = []
    for rule in RULES:
        if rule.startswith(struct):
            variation = rule.split(':')[1:]
            variations.append(variation)
            for i, var in enumerate(variation):
                if i >= len(classifiers):
                    classifiers.append([var])
                elif var not in classifiers[i]: 
                    classifiers[i].extend([var])
    return variations, classifiers

# returns all names that match the provided classifiers
def get_valid_names(classifiers):
    valid_names = []
    for _list in NAMES:
        if len(_list) > 0:
            if _list[0] in classifiers:
                for name in _list[1:]:
                    if name not in valid_names:
                        valid_names.append([name, _list[0]])
    return valid_names

# returns a list of optional parts/names for a provided variable, as defined in the rules. example: multiple possible spelling variants
def get_additives(param):
    result = []
    for _rule in RULES:
        rule = _rule.split(':')
        if rule[0] == 'additive' and rule[1] == param:
            result.extend(rule[2:])
    return result

# returns true if provided country code has a rules file
def valid_cc(var):
    if var in LANG_NEW: return True
    return False

# returns true if provided language has an options file
def valid_language(var):
    if var in [cc for language, cc in LANG_NEW.items()]: return True
    else: return False

# returns the country code for the provided language
def get_cc(var):
    for cc, language in LANG_NEW.items():
        if language == var:
            return cc

# returns the language for the provided country code
def get_language(var):
    if var in LANG_NEW:
        return LANG_NEW[var]

# sets the directory for the name database. hardcoded.
def set_dbdir():
    global DBDIR
    if not DBDIR == '': return True
    try:
        tmpdir = os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir))
        DBDIR = os.path.join(tmpdir, 'name_db/')
        return True
    except OSError: return False

# loads all languages from rules files
def load_lang():
    set_dbdir()
    for filename in os.listdir(DBDIR):
        filepath = os.path.join(DBDIR, filename)
        f = open(filepath)
        lang_set = f.readline().strip().split(':')
        if f.readline().strip().startswith('rules'): add_lang(lang_set)
        f.close()

# helper function - adds a country code and language to global list
def add_lang(lang):
    if len(lang) == 1:
        LANG_NEW[lang[0]] = lang[0] # fallback
    else: LANG_NEW[lang[0]] = lang[1]

# loads all names from lists that matches the provided country code
def loadDB(countrycode):
    for filename in os.listdir(DBDIR):
        filepath = os.path.join(DBDIR, filename)
        f = open(filepath)
        if f.readline().strip().split(':')[0] == countrycode:
            data = f.read().splitlines()
            build_list([line for line in data])
        f.close()

# clear lists, used when multiple tasks provided
def clear_lists():
    for _list in NAMES:
        _list.clear()
    RULES.clear()

# helper function - adds the contents of a rules file to the global rules list, or as a names list to the global names list
def build_list(data):
    if data[0] == 'rules':
        for line in data[1:]:
            if len(line) > 0: RULES.append(line)
    else:
        if not data[0] in CLASSIFIERS: CLASSIFIERS.extend(data[0])
        NAMES.append([line for line in data if len(line) > 0])


